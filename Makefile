generate: ## Generate go-files for protoc
generate:
	protoc -I proto proto/budget/budget.proto --go_out=./gen/go --go_opt=paths=source_relative --go-grpc_out=./gen/go --go-grpc_opt=paths=source_relative
